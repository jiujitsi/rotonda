/**
 *  Main JS file
 *  Based on https://live.autistici.org/ and https://plataformess.org/
 */


document.addEventListener("DOMContentLoaded", function(){

    fetch('settings.json').then(
        response => response.json()
    ).then(
        settings => {
            //Override settings if hash in url
            if (window.location.hash) {
              var name = window.location.hash.substr(1);
              settings['stream_mountpoint']='/' + name + '.m3u8';
              settings['chat_channel']=name;
              settings['html_title']='Live / ' + name;
              settings['html_description']='Live / ' + name;
            }
            //if user add #hash to url, reload the page
            window.addEventListener ('popstate', function (e) {
              console.log ('url changed');
              location.reload();
            });

            // HTML document
            document.title = settings.html_title;
            document.querySelector('meta[name=description]').content = settings.html_description;
            if(settings.logo_url){
                document.querySelector('.site-branding__link').href = settings.logo_url;
            }
            if(settings.logo_path){
                document.querySelector('.site-branding__logo').src = settings.logo_path;
            }else{
                document.querySelector('.site-branding__logo').parentNode.removeChild(document.querySelector('.site-branding__logo'));
            }
            if(settings.placeholder_image){
                document.querySelector('.placeholder__image').src = settings.placeholder_image;
            }else{
                document.querySelector('.placeholder__image').parentNode.removeChild(document.querySelector('.placeholder__image'));
            }
            // Chat
            var chat = document.querySelector('.chat-wrapper iframe');
            chat.src = settings.chat_server + "#" + settings.chat_channel;
            // Layout
            var chat_height = document.querySelector('.live-wrapper').offsetHeight;
            chat.style.height = chat_height-55 + "px";

            // Tips
            document.querySelector('.chat__title').addEventListener('click', function(){
                document.querySelector('.chat__helptext').classList.toggle('hidden');
            });
            document.querySelector('.chat__close').addEventListener('click', function(){
                document.querySelector('.chat__helptext').classList.add('hidden');
            });

            // Stream url
            var icecast_servers = settings.icecast_servers
            var streams = {}
            var stream_url;
            function get_stream_url(){
              for (var server of icecast_servers){
                fetch('https://' + server + '/status-json.xsl', {
                    method: 'GET'
                }).then(function(response){
                    return response.json()
                }).then(function(data) {
                    var sources = data['icestats']['source'];
                    var hostname = data['icestats']['host']
                    if (sources){
                      if (Array.isArray(sources)){
                        for (var mount of sources){
                          if (mount['listenurl'].includes(settings.stream_mountpoint)){
                            streams[hostname]=mount['listeners']
                          }
                        }
                      }else{
                        if (sources['listenurl'].includes(settings.stream_mountpoint)){
                            streams[hostname]=sources['listeners']
                        }
                      }
                    }else{
                      console.log('no live');
                    }
                }).then(function(data) {
                   //define stream_url when all icecast stats are parsed
                   if (icecast_servers.length == Object.keys(streams).length){
                     //console.log(streams)
                     const getMin = streams => {
                       return Object.keys(streams).filter(x => {
                         return streams[x] == Math.min.apply(null, 
                         Object.values(streams));
                       });
                     };
                     console.log('live from server: ' + getMin(streams)[0])
                     stream_url = 'https://' + getMin(streams)[0] + settings.stream_mountpoint;
                   }
                })
              }
            }

            // Timer
            var source = document.createElement("source");
            var video  = document.querySelector('.video-js');
            var streaming_system = settings.streaming_system;
            var placeholder  = document.querySelector('.placeholder');
            var video_is_running = false;
            if (streaming_system=='icecast'){
              setInterval(function(){
                //get status of icecast with CORS allowed
                fetch(settings.server_status_url, {
                    method: 'GET'
                }).then( function(response){
                    return response.text()
                }).then( function(data){
                    var running = data.indexOf(settings.stream_mountpoint) > -1;
                    if(running){
                        get_stream_url();
                        if(!video_is_running && stream_url){
                          // Video
                          source.setAttribute("type", settings.live_mimetype);
                          source.setAttribute("src", stream_url);
                          video.appendChild(source);
                          video.play();
                          video_is_running = true;
                          placeholder.classList.add("hidden");
                          document.body.classList.remove("showing-placeholder");
                          // Layout
                          var chat_height = document.querySelector('.live-wrapper').offsetHeight;
                          chat.style.height = Math.max(chat_height, 500)-55 + "px";
                        }
                    } else {
                        video_is_running = false;
                        placeholder.classList.remove("hidden");
                        document.body.classList.add("showing-placeholder");
                    }
                });
              }, 2000);
            } else {
              if (streaming_system=='nginx' && settings.server_status_url){

                //nginx stats available
                setInterval(function(){
                  //get status of nginx rtmp with CORS allowed
                  fetch(settings.server_status_url, {
                      method: 'GET'
                  }).then( function(response){
                      return response.text()
                  }).then( function(data){
                      mount = settings.stream_mountpoint.replace('/','').replace('.m3u8','');
                      var running = data.indexOf('<name>' +mount+ '</name>') > -1;
                      if(running){
                        stream_url = settings.nginx_server + settings.stream_mountpoint;
                        if(!video_is_running && stream_url){
                          // Video
                          setTimeout(function(){
                            //var v = videojs('videoid');
                            var v = window.player = videojs('videoid', {
                              html5: {
                                hls: {
                                  overrideNative: true
                                },
                                nativeAudioTracks: false,
                                nativeVideoTracks: false
                              }
                            });
                            v.src({type: settings.live_mimetype, src: stream_url});
                            video_is_running = true;
                            placeholder.classList.add("hidden");
                            document.body.classList.remove("showing-placeholder");
                            v.ready(function() {
                              if (v.paused()) {
                                v.play();
                              }
                              v.on('error', function() {
                                //force to reconnect on errors
                                video_is_running = false;
                              });
                              //reconnect on hls playlist warn
                              v.tech(true).on('retryplaylist', (event) => {
                                console.log('retry')
                                v.reset();
                                v.src({type: settings.live_mimetype, src: stream_url});
                                v.load();
                                v.play();
                              });
                            });
                            // Layout
                            var chat_height = document.querySelector('.live-wrapper').offsetHeight;
                            chat.style.height = Math.max(chat_height, 500)-55 + "px";
                          }, 4000);
                        }
                      } else {
                        video_is_running = false;
                        placeholder.classList.remove("hidden");
                        document.body.classList.add("showing-placeholder");
                        var chat_height = document.querySelector('.live-wrapper').offsetHeight;
                        chat.style.height = Math.max(chat_height, 500)-55 + "px";
                      }
                  });
                }, 5000);

              } else {
                //nginx stats not allowed due to CORS
                stream_url = settings.nginx_server + settings.stream_mountpoint;
                // Video
                var v = videojs('videoid');
                v.src({type: settings.live_mimetype, src: stream_url});
                video_is_running = true;
                setTimeout(function(){ placeholder.classList.add("hidden"); }, 5000);
                document.body.classList.remove("showing-placeholder");
                // Layout
                var chat_height = document.querySelector('.live-wrapper').offsetHeight;
                chat.style.height = Math.max(chat_height, 500)-55 + "px";
              }
            }
        }
    );

});
